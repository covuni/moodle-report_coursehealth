<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

/*
 *
 * @package report
 * @subpackage coursehealth
 * @copyright 2018 Kevin Moore (Coventry University)
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once(__DIR__ . '/reportColumn.php');

/*
 * Average hits per user.
 * Find the users (students) enrolled in the course.
 * Then ratio to the CRUD activity counts in this course as held in the report_coursehealth table.
 */
class averagehitsperuser extends reportColumn
{
    public function __construct($context) {
        parent::__construct($context, 'averagehits');
    }

    /**
     * Do whatever analysis is needed for this datapoint using this courseid.
     * Store the data in array indexed against the courseid.
     *
     * @param unknown $courseid

     */
    public function analyse($courseid, $fullname = '') {
        global $DB;

        $total = $DB->get_field('report_coursehealth', 'all_counter', ['courseid' => $courseid]);

        // The record was not found, we create it so that the scheduled task will collecting information for this course.
        if ($total === false) {
            $record = new stdClass();
            $record->courseid = $courseid;
            $DB->insert_record('report_coursehealth', $record);
            parent::$data[$courseid][$this->key] = [$this->key => 0];
            return;
        }

        $context = context_course::instance($courseid);
        $roleconfig = get_config('report_coursehealth', 'studentroleids');
        $roleids = array_map('intval', explode(',', (string) $roleconfig));

        list($roleidsql, $roleidparams) = $DB->get_in_or_equal($roleids, SQL_PARAMS_NAMED, 'param', true);

        $studentcount = $DB->count_records_select(
            'role_assignments',
            "contextid = :contextid AND roleid $roleidsql",
            ['contextid' => $context->id] + $roleidparams,
            'COUNT(DISTINCT userid)'
        );

        $avghits = 0;
        if ($studentcount > 0) {
            $avghits = $total / $studentcount;
        }

        parent::$data[$courseid][$this->key] = [$this->key => $avghits];
    }

    /**
     * float value from analysis or -1 is no guide at all.
     */
    public function decorate_table($courseid) {
        $datapoint = parent::$data[$courseid][$this->key][$this->key];

        if ($datapoint == 0) { // No statistics.
            return $this->scheduledForLatericon();
        } else {
            if ($datapoint < 1) {
                return "~ 0";
            }
            if ($datapoint > 1000) {
                return "> 1000";
            }
            return number_format($datapoint, 0);
        }
    }

    public function decorate_xls($courseid) {
        $datapoint = parent::$data[$courseid][ $this->key ][$this->key];
        return $datapoint;
    }
}
