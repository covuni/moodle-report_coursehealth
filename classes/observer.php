<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Event observer.
 *
 * @package report_coursehealth
 * @copyright 2018 Coventry University
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @author Kevin Moore <ac4581@coventry.ac.uk> Coventry University
 */

namespace report_coursehealth;

defined('MOODLE_INTERNAL') || die();

/**
 * Event observer.
 */
class observer {

    /**
     * Observes assignment workflow state changes.
     */
    public static function workflow_state_updated(\mod_assign\event\workflow_state_updated $event) {
        global $DB;

        $courseid = $event->courseid;
        $studentid = $event->relateduserid;
        $assignid = $event->objectid;
        $timecreated = $event->timecreated;

        // We only care about state changes when they are going to be marked as released.
        if ($event->other['newstate'] != ASSIGN_MARKING_WORKFLOW_STATE_RELEASED) {
            return true;
        }

        // Create simple record and bail if it already exists.
        $record = new \stdClass();
        $record->courseid = $courseid;
        $record->studentid = $studentid;
        $record->assignid = $assignid;
        if ($DB->record_exists('report_coursehealth_released', (array) $record)) {
            return true;
        }

        // Append the time at which we captured this, and insert the record.
        $record->timecreated = $timecreated;
        $DB->insert_record('report_coursehealth_released', $record);

        return true;
    }

}
