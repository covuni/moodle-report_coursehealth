<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

/*
 *
 * @package   	report
 * @subpackage 	coursehealth
 * @copyright   2018 Kevin Moore <ac4581@coventry.ac.uk> Coventry University
 * @license   	http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once("$CFG->libdir/formslib.php");

/**
 * Create a form that offers filters that will control the report generated.
 * @author <ac4581@coventry.ac.uk> Kevin Moore Coventry University
 */
class coursehealthform extends moodleform {

    /** @var The list of keys and their type and default values. */
    public static $reportkeys = [
        ['category', PARAM_INT, 0],
        ['includesubcats', PARAM_INT, 0],
        ['filtername', PARAM_RAW, ''],
        ['filtercoursehidden', PARAM_INT, 0],
        ['includewithoutstudents', PARAM_INT, 0],
        ['turnaroundskipgreaterthan', PARAM_INT, 0],
    ];

    // Add elements to form.
    public function definition() {
        global $CFG;

        $mform = $this->_form; // Don't forget the underscore!

        $mform->addElement('header', 'coursefilterhdr', get_string('filter', 'core'));
        $mform->addElement('hidden', 'filterformsubmitted', 1);
        $mform->setType('filterformsubmitted', PARAM_BOOL);

        // Select box of top level categories.
        $contents = core_course_category::make_categories_list();
        $attributes = array();
        $mform->addElement('select', 'category', get_string('filtercategories', 'report_coursehealth'), $contents, $attributes);
        $mform->addHelpButton('category', 'filtercategories', 'report_coursehealth' );

        // Checkbox for whether or not sub categories should be included.
        $mform->addElement('advcheckbox', 'includesubcats', null, get_string('includesubcats', 'report_coursehealth'));
        $mform->setDefault('includesubcats', false);

        // Text entry for filters on fullname TODO mark this as optional?
        $attributes = array('size' => '20');
        $mform->addElement('text', 'filtername', get_string('filterfullname', 'report_coursehealth'), $attributes);
        $mform->addHelpButton('filtername', 'filterfullname', 'report_coursehealth' );
        $mform->setType('filtername', PARAM_TEXT);

        // Checkboxes of course atrributes eg hidden issue #16.
        $mform->addElement('advcheckbox', 'filtercoursehidden',
            get_string('filtercoursetitle', 'report_coursehealth'),
            get_string('filtercoursehidden', 'report_coursehealth'));
        $mform->setDefault('filtercoursehidden', 0);

        $mform->addElement('advcheckbox', 'includewithoutstudents', '',
            get_string('includewithoutstudents', 'report_coursehealth'));
        $mform->setDefault('includewithoutstudents', 0);

        $mform->addElement('header', 'columnsettingshdr', get_string('columnsettings', 'report_coursehealth'));

        $mform->addElement('duration', 'turnaroundskipgreaterthan', get_string('turnaroundskipgreaterthan', 'report_coursehealth'));
        $mform->addHelpButton('turnaroundskipgreaterthan', 'turnaroundskipgreaterthan', 'report_coursehealth');
        $mform->setDefault('turnaroundskipgreaterthan', 30 * DAYSECS);

        $this->add_action_buttons(true, get_string('generatereport', 'report_coursehealth'));
    }

    // Custom validation should be added here.
    public function validation($data, $files) {
        return array();
    }

}