<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

/*
 *
 * @package report
 * @subpackage coursehealth
 * @copyright 2018 Kevin Moore (Coventry University)
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once(__DIR__ . '/reportColumn.php');

/**
 * "Moderator guide" is the blocks/moderator_guide block plugin.
 * Stores the analysis, 1 means completed, 0 is not completed, -1 meaning the block doesnt exist in the course.
 *
 * Block > HLS Moderator Guide > edit guide > mark completed at the bottom part of form.
 *
 * @author Kevin Moore <ac4581@coventry.ac.uk> Coventry University
 *
 */
class moderatorGuideStaff extends reportColumn
{

    private $context;

    public function __construct($context) {
        parent::__construct($context, 'moderatorguidestaff');
    }

    /**
     * Does the guide plugin exist on this courseid?
     * Store the data in array indexed against the courseid.
     *
     * @param unknown $courseid
     */
    public function analyse($courseid, $fullname='') {
        global $DB;

        $ret = - 1; // Not present.

        if ($DB->get_manager()->table_exists('block_mdrtr_guide_guides') ) {

            // IGNORE_MISSING means compatible mode, false returned if record not found, debug message if more found.
            $res = $DB->get_record( 'block_mdrtr_guide_guides', array('courseid' => "$courseid"), IGNORE_MISSING );

            if (! empty($res) && $res != false) { // Guide added to this course.
                $ret = $res->completed;
            }
        }
        parent::$data[$courseid][$this->key] = array( $this->key => $ret );
    }

    public function decorate_table($courseid) {
        $datapoint = parent::$data[$courseid][$this->key][$this->key];

        if ($datapoint == - 1) { // No guide.
            return $this->notApplicableicon();
        } else {
            return $this->returnCompletedicon($datapoint);
        }

    }

    public function decorate_xls($courseid) {
        $datapoint = parent::$data[$courseid][$this->key][$this->key];
        return $datapoint;
    }
}
