<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Reports the course visibility.
 *
 * @package report_coursehealth
 * @copyright 2019 Coventry University
 * @author Frédéric Massart <fred@branchup.tech>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace report_coursehealth\local\column;
defined('MOODLE_INTERNAL') || die();

/**
 * Reports the course visibility.
 *
 * @package report_coursehealth
 * @copyright 2019 Coventry University
 * @author Frédéric Massart <fred@branchup.tech>
 */
class visibility extends base {

    public function __construct($context) {
        parent::__construct($context, 'visibility');
    }

    /**
     * Load the visibility state.
     *
     * @param int $courseid The course ID.
     * @param string $fullname The full name.
     */
    public function analyse($courseid, $fullname='') {
        global $DB;
        $visible = $DB->get_field('course', 'visible', ['id' => $courseid], IGNORE_MISSING);
        parent::$data[$courseid][$this->key] = $visible;
    }

    public function decorate_table($courseid) {
        $visible = parent::$data[$courseid][$this->key];
        if ($visible === false) {
            return '?';
        }
        return $visible ? get_string('yes') : get_string('no');
    }

    public function decorate_xls($courseid) {
        $visible = parent::$data[$courseid][$this->key];
        if ($visible === false) {
            return '';
        }
        return $visible ? 1 : 0;
    }
}
