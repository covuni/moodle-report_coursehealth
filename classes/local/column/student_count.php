<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Reports the course student count.
 *
 * @package report_coursehealth
 * @copyright 2019 Coventry University
 * @author Frédéric Massart <fred@branchup.tech>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace report_coursehealth\local\column;
defined('MOODLE_INTERNAL') || die();

use context_course;

/**
 * Reports the course student count.
 *
 * @package report_coursehealth
 * @copyright 2019 Coventry University
 * @author Frédéric Massart <fred@branchup.tech>
 */
class student_count extends base {

    public function __construct($context) {
        parent::__construct($context, 'studentcount');
    }

    /**
     * Analyse.
     *
     * @param int $courseid The course ID.
     * @param string $fullname The full name.
     */
    public function analyse($courseid, $fullname='') {
        global $DB;

        // Get the roles. When the setting is not defined, role IDs are [0].
        $roleconfig = get_config('report_coursehealth', 'studentroleids');
        $roleids = array_map('intval', explode(',', (string) $roleconfig));

        // Find out the number of users who were assigned a student in the course.
        // At this point this does not check whether the users have a valid, active
        // enrolment within this course.
        list($roleidsql, $roleidparams) = $DB->get_in_or_equal($roleids, SQL_PARAMS_NAMED);
        $sql = "SELECT COUNT(DISTINCT ra.userid)
                  FROM {role_assignments} ra
                 WHERE ra.contextid = :contextid
                   AND ra.roleid $roleidsql";
        $params = $roleidparams + ['contextid' => context_course::instance($courseid)->id];
        $data = $DB->count_records_sql($sql, $params);
        parent::$data[$courseid][$this->key] = $data;
    }

    public function decorate_table($courseid) {
        $data = parent::$data[$courseid][$this->key];
        if ($data === false) {
            return '?';
        }
        return $data;
    }

    public function decorate_xls($courseid) {
        $data = parent::$data[$courseid][$this->key];
        if ($data === false) {
            return '';
        }
        return $data;
    }
}
