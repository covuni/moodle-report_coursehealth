<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

/*
 *
 * @package report
 * @subpackage coursehealth
 * @copyright 2018 Kevin Moore (Coventry University)
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * This is the base class for each column of the report. Derive from here to give a column the ability to do an analysis and
 * to display its results.
 *
 * @author Kevin Moore <ac4581@coventry.ac.uk> Coventry University
 *
 */
abstract class reportColumn
{

    private $context;

    /**
     * Unique ID for this column, used to identify tables columns, index data, collect language strings.
     */
    public $key;

    /**
     * Array of collected datapoints, an array indexed by courseid. Each datapoint is an array whth at least one element $key=>x
     * All of the plugins collate thier data into this, hence static.
     */
    protected static $data;

    /**
     * @param unknown $context
     * @param string $key Derived classes should set a unique identity for this column
     */
    public function __construct($context, $key='auniquekey') {
        global $OUTPUT;

        self::$data = array();    // Initialise global store of data.
        $this->context = $context;
        $this->key = $key;

    }

    /**
     * Perform the required analysis of the specified course, saving results (an array whatever is wanted) into
     * $this->data[$courseid][$this->key]
     *
     * @param unknown $courseid
     * @param string $fullname
     */
    abstract public function analyse($courseid, $fullname='');
    /**
     * Take the data collected for this courseid and create a nice representation for output to HTML table
     * @param unknown $courseid
     */
    abstract public function decorate_table($courseid);
    /**
     * Take the data collected for this courseid and create a nice representation for output to Excel
     * @param unknown $courseid
     */
    abstract public function decorate_xls($courseid);

    protected function scheduledforlatericon() {
        global $OUTPUT;
        return $OUTPUT->pix_icon("i/scheduled",
                                  get_string('rollover.scheduled', 'report_coursehealth')); // Grey clock.
    }

    protected function notapplicableicon() {
        global $OUTPUT;
        return $OUTPUT->pix_icon("i/completion-auto-n",
                                  get_string('rollover.notapplicable', 'report_coursehealth')); // Empty grey square.
    }

    /**
     * Use when something is wrong eg expecting one forum but found more
     */
    protected function warningicon() {
        global $OUTPUT;
        return $OUTPUT->pix_icon("i/warning",
            get_string('rollover.warning', 'report_coursehealth')); // Grey triangle.
    }
    /**
     * Send a 1 to use the 'correct' icon, anything else gives the incorrect icon
     *

     * @param unknown $bool

     * @return pix_icon
     */
    protected function returncompletedicon($bool) {
        global $OUTPUT;
        return $bool == 1 ? $OUTPUT->pix_icon('i/grade_correct', get_string('rollover.completed', 'report_coursehealth')) :
                            $OUTPUT->pix_icon('i/grade_incorrect', get_string('rollover.notcompleted', 'report_coursehealth'));
    }

    /**
     * This will look for language strings in the pattern  "header.$key" (key as provided in the constructor)
     * and return a full HTML snippet for inclusion in table.
     * @return string
     */
    public function getheaderhtml() {
        global $OUTPUT;
        $output = $OUTPUT; // TODO need a renderer report_coursehealth?

        return $this->getheader() .
                           $output->help_icon('header.'.$this->key, 'report_coursehealth');
    }

    public function getheader() {
        return get_string('header.'.$this->key, 'report_coursehealth');
    }

    /**
     * Whether the column appears in downloads.
     *
     * @return bool
     */
    public function is_shown_on_download() {
        return true;
    }

    /**
     * Whether the column appears in downloads.
     *
     * @return bool
     */
    public function is_shown_on_web() {
        return true;
    }

}
