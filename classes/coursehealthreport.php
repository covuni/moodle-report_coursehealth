<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die;

/*
 *
 * @package report
 * @subpackage coursehealth
 * @copyright 2018 Kevin Moore <ac4581@coventry.ac.uk> Coventry University
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once($CFG->libdir . '/adminlib.php');
require_once($CFG->libdir . '/tablelib.php');
require_once($CFG->libdir . '/formslib.php');
require_once($CFG->libdir . '/filelib.php');
require_once($CFG->libdir . '/filelib.php');
require_once(__DIR__ . '/course.php');
require_once(__DIR__ . '/yousaidwedid.php');
require_once(__DIR__ . '/averagehits.php');
require_once(__DIR__ . '/moderatorGuideStaff.php');
require_once(__DIR__ . '/moduleInformation.php');
require_once(__DIR__ . '/countAnnouncements.php');
require_once(__DIR__ . '/countDiscussions.php');
require_once(__DIR__ . '/assignmentTurnaround.php');
require_once(__DIR__ . '/echoUsage.php');
require_once(__DIR__ . '/assignmentTurnaroundCount.php');
require_once(__DIR__ . '/assignmentTurnaroundDetails.php');

/**
 * Does the legwork to run the analysers and decorators and present to screen or other formats.
 * This is where we have a list of the columns in the report - adding a new column involves:
 *  <ul>
 *      <li>creating a class that derives from reportColumn, create abstract methods</li>
 *      <li>require the class in this php</li>
 *      <li>add an instance of the class to the $reportcolumns array</li>
 *  </ul>
 *
 * @author <ac4581@coventry.ac.uk> Kevin Moore, Coventry University
 *
 */
class coursehealthreport
{

    private $context;

    /**
     * The courseids to be analaysed for this report, see collectData()
     */
    private $courseids;

    /**
     * Array of reportcolumn objects, the order will determine the ordering of output tables.
     */
    private $reportcolumns;

    public function __construct($context, $submitteddata) {
        $this->context = $context;

        // trigger_error( "form submitted " . print_r( $submitteddata, true) );

        $this->courseids = array();

        // This should be the only place where we define which columns to display.
        $this->reportcolumns = array();
        $this->reportcolumns['course'] = new course($context);
        $this->reportcolumns['yousaidwedid'] = new yousaidwedid($context);
        $this->reportcolumns['averagehits'] = new averagehitsperuser($context);
        $this->reportcolumns['moderatorguidestaff'] = new moderatorGuideStaff($context);
        $this->reportcolumns['moduleinformation'] = new moduleInformation($context);
        $this->reportcolumns['announcements'] = new countAnnouncements($context);
        $this->reportcolumns['discussions'] = new countDiscussions($context);
        $this->reportcolumns['assignmentturnaround'] = new assignmentTurnaround($context, $submitteddata);
        $this->reportcolumns['assignmentturnaroundcount'] = new assignmentTurnaroundCount($context);
        $this->reportcolumns['echousage'] = new echoUsage($context);
        $this->reportcolumns['assignmentturnarounddetails'] = new assignmentTurnaroundDetails($context);
        $this->reportcolumns['visibility'] = new report_coursehealth\local\column\visibility($context);
        $this->reportcolumns['studentcount'] = new report_coursehealth\local\column\student_count($context);

        $sort = 'course';
        $conditions = get_object_vars($submitteddata);
        $this->collectdata($conditions, $sort);

    }

    /**
     * Run the queries to obtain raw data into an array(row) of arrays(column) of arrays(dataset for column).
     *
     * @param array $conditions - hashed array of database column=>required value
     * @param string $sort
     * @return mixed[][] - rows of data. Each row is an array of columns.
     */
    private function collectdata( $conditions, $sort ) {
        global $DB;

        $ret = array(); // Will become an array of arrays.

        $categoryid = $conditions['category'];
        $includesubcats = !empty($conditions['includesubcats']);
        $includewithoutstudents = !empty($conditions['includewithoutstudents']);

        // Get a list of courses we want to analyse.
        $selectsql = "SELECT c.id, c.shortname, c.fullname, c.visible
                        FROM {course} c
                        JOIN {course_categories} cc
                          ON c.category = cc.id
                        JOIN {context} ctx
                          ON ctx.instanceid = c.id
                         AND ctx.contextlevel = :contextlevel";
        $wheres = [];
        $params = ['contextlevel' => CONTEXT_COURSE];

        // We filter out courses without students, unless the report specifically includes them.
        if (!$includewithoutstudents) {
            $roleconfig = get_config('report_coursehealth', 'studentroleids');
            $roleids = array_map('intval', explode(',', (string) $roleconfig));
            list($roleidsql, $roleidparams) = $DB->get_in_or_equal($roleids, SQL_PARAMS_NAMED);
            $wheres[] = "EXISTS (
                SELECT 1
                  FROM {role_assignments} ra
                 WHERE ra.contextid = ctx.id
                   AND ra.roleid $roleidsql
            )";
            $params += $roleidparams;
        }

        if (!$includesubcats) {
            $wheres[] = 'c.category = :categoryid';
            $params['categoryid'] = $categoryid;
        } else {
            $category = core_course_category::get($categoryid);
            $wheres[] = '(cc.id = :categoryid OR ' . $DB->sql_like('cc.path', ':categorypath') . ')';
            $params['categoryid'] = $categoryid;
            $params['categorypath'] = $DB->sql_like_escape($category->path . '/') . '%';
        }

        // If we have been passed a filter.
        if (!empty($conditions['filtername'])) {
            $params['filtername'] = '%' . $DB->sql_like_escape($conditions['filtername']) . '%';
            $wheres[] = $DB->sql_like('c.fullname', ':filtername', false);
        }

        // If we have a coursefilter.
        if (empty($conditions['filtercoursehidden'])) {
            $wheres[] = 'c.visible = 1';
        }

        $sql = $selectsql . ' WHERE ' . implode(' AND ', $wheres);
        $rs = $DB->get_recordset_sql($sql, $params);
        foreach ($rs as $courserecord) {
            // For each course, ask each columnreport to collect and store the data.

            $this->courseids[] = $courserecord->id;
            foreach ($this->reportcolumns as $rc) {
                // trigger_error("column = " . print_r($rc,true) );
                $rc->analyse($courserecord->id, $courserecord->fullname );
            }
        }
        $rs->close(); // Don't forget to close the recordset!

        return $ret;
    }

    /**
     * Generate a moodle flexible_table of formatted results to screen.
     * Results are formatted according to the decorator for each column.
     *
     * @param $url moodle_url The base URL.
     * @param string $download The download plugin.
     */
    public function table(moodle_url $url, $download = null) {
        $isdownloading = !empty($download);

        $columns = array_filter($this->reportcolumns, function($c) use ($isdownloading) {
            return $isdownloading ? $c->is_shown_on_download() : $c->is_shown_on_web();
        });

        $headers = array_map(function($rc) use ($isdownloading) {
            return $isdownloading ? $rc->getHeader() : $rc->getHeaderHTML();
        }, $columns);

        // Construct a HTML sortable table.
        $table = new flexible_table('coursehealth_table');
        $table->set_attribute('class', 'report_coursehealth_table');
        $table->define_columns(array_keys($columns)); // Unique names (keys) for columns.
        $table->define_headers(array_values($headers));
        $table->define_baseurl($url);
        $table->is_downloadable(true);
        $table->is_downloading($download, 'report');
        $table->sortable(false, 'course'); // Sorting causes a call back to url :( ...
        $table->collapsible(false); // Columns can be hidden - actually calls the base url again.
        $table->setup();

        foreach ($this->courseids as $courseid) {
            $tablerow = array();
            foreach ($columns as $rc) {
                $content =  $isdownloading ? $rc->decorate_xls($courseid) : $rc->decorate_table($courseid);
                $tablerow[$rc->key] = $content;
            }
            $table->add_data_keyed($tablerow);
        }
        $table->finish_output();
    }

    /**
     * Create results and save to an XLS file for download.
     *

     * @param unknown $submitteddata

     */
    public function xls() {
        global $USER;

        $fs = get_file_storage();

        // Prepare file record object.
        $fileinfo = array(
            'contextid' => $this->context->id, // ID of context.
            'component' => 'report_coursehealth', // Usually = table name.
            'filearea' => 'xlsreport', // Usually = table name.
            'itemid' => 0, // usually = ID of row in table.
            'filepath' => '/', // any path beginning and ending in '/' .
            'filename' => 'report' . $USER->id.'.xls'
        );

        // Get file to see if it exits, delete.
        $file = $fs->get_file($fileinfo['contextid'], $fileinfo['component'], $fileinfo['filearea'],
            $fileinfo['itemid'], $fileinfo['filepath'], $fileinfo['filename']);
        if ($file) {
            $file->delete();
        }

        // Read the data and parse into CSV.
        // TODO can we get this into a proper XLS template? Then we could have ready made charts etc.
        $str = "Report generated " . date("Y-m-d H:i:s") . PHP_EOL;

        // Add in the column headers using their display name from the lang file ie without HTML wrapping.
        $headers = array();
        foreach ($this->reportcolumns as $rc) {
            $headers[] = $rc->getHeader();
        }
        $str .= implode( ',', $headers ) . PHP_EOL;

        foreach ($this->courseids as $courseid) {
            $tablerow = array();

            foreach ($this->reportcolumns as $rc) {
                $tablerow[] = $rc->decorate_xls($courseid); // Pickout this columns' dataset and give to decorator.
            }
            $str .= implode( ',', $tablerow ) . PHP_EOL;
        }

        // Create file, adding a record into files table with column component=report_coursehealth.
        $fs->create_file_from_string($fileinfo, $str);

        // Return the URL using the pluginfile.php utility.
        return $url = moodle_url::make_pluginfile_url($fileinfo['contextid'], $fileinfo['component'],
                                                      $fileinfo['filearea'], $fileinfo['itemid'], $fileinfo['filepath'],
                                                      $fileinfo['filename']);
    }
}