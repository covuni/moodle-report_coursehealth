<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

/**
 * Assignment turn around details.
 *
 * @package report_coursehealth
 * @copyright 2019 Coventry University
 * @author Frédéric massart <fred@branchup.tech>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once(__DIR__ . '/reportColumn.php');

/**
 * Assignment turn around details.
 *
 * This depends on the assignmentTurnaround column. The main purpose of this colum
 * is to display details as a separate column when the report is downloaded.
 *
 * @package report_coursehealth
 * @copyright 2019 Coventry University
 * @author Frédéric massart <fred@branchup.tech>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class assignmentTurnaroundDetails extends reportColumn {

    public function __construct($context) {
        parent::__construct($context, 'assignmentturnarounddetails');
    }

    public function analyse($courseid, $fullname = '') {
       return;
    }

    public function decorate_table($courseid) {
        return '';
    }

    public function decorate_xls($courseid) {
        $datapoint = parent::$data[$courseid]['assignmentturnaround']['turnaroundassignments'];
        return implode("\n", array_map(function($assign) {
                $days = $assign->averageturnaroundsecs / 86400;
                return "{$assign->name}: {$days}; ";
            }, array_filter($datapoint, function($assign) {
                return !empty($assign->released);
            })
        ));
    }

    public function is_shown_on_web() {
        return false;
    }
}
