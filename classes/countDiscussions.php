<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

/**
 * The number of posts in a course.
 *
 * @package report_coursehealth
 * @copyright 2018 Kevin Moore (Coventry University)
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(__DIR__ . '/reportColumn.php');

/**
 * Reports on the number of posts in a course, excluding announcements.
 *
 * @author Kevin Moore <ac4581@coventry.ac.uk> Coventry University
 */
class countDiscussions extends reportColumn {

    public function __construct($context) {
        parent::__construct($context, 'discussions');
    }

    /**
     * Locate and process all the forum posts that are not announcements.
     *
     * @param int $courseid The course ID.
     * @param string $fullname The full name.
     */
    public function analyse($courseid, $fullname='') {
        global $DB;

        $sql = "SELECT COUNT(d.id)
                  FROM {forum_posts} p
                  JOIN {forum_discussions} d
                    ON p.discussion = d.id
                  JOIN {forum} f
                    ON d.forum = f.id
                 WHERE f.course = :courseid
                   AND f.type != :type";
        $count = $DB->count_records_sql($sql, ['courseid' => $courseid, 'type' => 'news']);

        parent::$data[$courseid][$this->key] = [$this->key => $count];
    }

    public function decorate_table($courseid) {
        return parent::$data[$courseid][$this->key][$this->key];
    }

    public function decorate_xls($courseid) {
        return parent::$data[$courseid][$this->key][$this->key];
    }
}
