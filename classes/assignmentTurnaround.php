<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

/**
 * Assignment turn around.
 *
 * @package report_coursehealth
 * @copyright 2018 Kevin Moore (Coventry University)
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(__DIR__ . '/reportColumn.php');

/**
 * The average duration between an assignment due date and the grade release.
 *
 * Only submissions made are included in the calculations,
 * hence if an assignment has 7 students but only 5 make submissions the average turnaround
 * is the interval between the assignment due date and the grade release on those 5 submissions.
 * Otherwise a failure would submit would make the average infinite.
 * This calculation is repeated for each assignment on the course and averaged.
 *
 * The grade release is a trapped event that populates a DB table. This class performs the analytics on that
 * data.
 * @see report_coursehealth/observer.php
 *
 *  see https://moodle.org/mod/forum/discuss.php?d=327568
 *
 * @author Kevin Moore <ac4581@coventry.ac.uk> Coventry University
 *
 */
class assignmentTurnaround extends reportColumn {

    /** @var bool Whether the page was initialised. */
    protected $pageinitialised = false;
    /** @var int Skip the turn arounds greaten these seconds than whe computing average. */
    protected $skipturnaroundgreaterthan;

    public function __construct($context, $reportsettings) {
        parent::__construct($context, 'assignmentturnaround');
        $this->skipturnaroundgreaterthan = $reportsettings->turnaroundskipgreaterthan;
    }

    /**
     * Calculate the average turn around time.
     *
     * @param unknown $courseid
     */
    public function analyse($courseid, $fullname = '') {
        global $DB;

        $ceilavg = $DB->sql_ceil('AVG(timecreated)');
        $sql = "SELECT a.id, cm.id AS cmid, a.name, a.duedate, COUNT(ch.id) AS releasedcount, $ceilavg AS averagedate
                  FROM {assign} a
                  JOIN {modules} m
                    ON m.name = :assign
                  JOIN {course_modules} cm
                    ON cm.module = m.id
                   AND cm.instance = a.id
             LEFT JOIN {report_coursehealth_released} ch
                    ON ch.assignid = a.id
                 WHERE a.course = :courseid
                   AND a.markingworkflow = 1
                   AND a.duedate > 0
              GROUP BY a.id, cm.id, a.name, a.duedate";
        $params = [
            'courseid' => $courseid,
            'assign' => 'assign'
        ];

        // Log each assignment that match our criteria.
        $report = [];
        $assignments = $DB->get_recordset_sql($sql, $params);
        foreach ($assignments as $assign) {
            $report[] = (object) [
                'id' => $assign->id,
                'cmid' => $assign->cmid,
                'name' => $assign->name,
                'duedate' => $assign->duedate,
                'released' => $assign->releasedcount,
                'averagedate' => $assign->averagedate,
                'averageturnaroundsecs' => $assign->releasedcount > 0 ? max(0, $assign->averagedate - $assign->duedate) : 0
            ];
        }
        $assignments->close();

        // Count the total number of assignments released.
        $totalassignmentsreleased = count(array_filter($report, function($assign) {
            return !empty($assign->released);
        }));

        // Compute the turn around metrics.
        $turnaroundassignments = array_filter($report, function($assign) {
            return !$this->should_skip_assignment($assign);
        });
        list($turnaroundassignmentsreleased, $totalsecs) = array_reduce($turnaroundassignments,
            function($carry, $assign) {
                return [$carry[0] + 1, $carry[1] + $assign->averageturnaroundsecs];
            },
            [0, 0]
        );
        $overallaveragesecs = $turnaroundassignmentsreleased > 0 ? $totalsecs / $turnaroundassignmentsreleased : -1;

        parent::$data[$courseid][$this->key] = [
            'mean' => $overallaveragesecs >=0 ? $overallaveragesecs / 86400 : $overallaveragesecs,
            'assignmentcount' => count($report),
            'assignmentreleased' => $totalassignmentsreleased,
            'assignments' => $report,   // All the assignments.
            'turnaroundassignments' => $turnaroundassignments,  // The assignments matching the turn around skips.
        ];
    }

    /**
     * Whether we should skip the assignment for turn around related info.
     *
     * @param stdClass $assign As per the analyse method.
     * @return bool
     */
    public function should_skip_assignment($assign) {
        return !$assign->released
            || ($this->skipturnaroundgreaterthan && $assign->averageturnaroundsecs > $this->skipturnaroundgreaterthan);
    }

    /**
     * Decorate the table.
     *
     * @param int $courseid
     * @return string
     */
    public function decorate_table($courseid) {
        global $OUTPUT;

        $this->init_page_requirements();

        $context = context_course::instance($courseid);
        $datapoint = parent::$data[$courseid][$this->key]['mean'];
        $assignments = parent::$data[$courseid][$this->key]['turnaroundassignments'];

        if ($datapoint == -1) {
            return '-';
        }

        $contentid = html_writer::random_id();
        $o = '';
        $o .= html_writer::start_tag('div', ['class' => 'assign-turnaround-cell collapsed']);
        $o .= html_writer::start_tag('div');
        $o .= $this->format_days($datapoint);
        $o .= html_writer::start_tag('a', [
            'href' => '#',
            'class' => 'assign-turnaround-expand-actions',
            'role' => 'button',
            'aria-controls' => $contentid
        ]);
        $o .= $OUTPUT->pix_icon('t/collapsed', get_string('expand', 'core'), 'core', ['class' => 'expand-action']);
        $o .= $OUTPUT->pix_icon('t/expanded', get_string('expand', 'core'), 'core', ['class' => 'collapse-action']);
        $o .= html_writer::end_tag('a');
        $o .= html_writer::end_tag('div');

        $o .= html_writer::start_tag('div', ['class' => 'assign-turnaround-collapsible-content', 'id' => $contentid]);
        $o .= html_writer::start_tag('ul');

        foreach ($assignments as $assign) {
            $link = html_writer::link(
                new moodle_url('/mod/assign/view.php', ['id' => $assign->cmid]),
                format_string($assign->name, true, ['context' => $context])
            );
            $o .= html_writer::start_tag('li');
            $o .= get_string('acolonb', 'report_coursehealth', [
                'left' => $link,
                'right' => $this->format_days($assign->averageturnaroundsecs / 86400)
            ]);
            $o .= html_writer::end_tag('li');
        }

        $o .= html_writer::end_tag('ul');
        $o .= html_writer::end_tag('div');
        return $o;
    }

    public function decorate_xls($courseid) {
        return parent::$data[$courseid][ $this->key ]['mean'];
    }

    /**
     * Format days.
     *
     * @param int $days The numbero f days.
     * @return string
     */
    protected function format_days($days) {
        $langstring = $days > 1 ? 'numdays' : 'numday';
        return get_string($langstring, 'core', format_float($days, 2));
    }

    protected function init_page_requirements() {
        global $OUTPUT, $PAGE;

        if ($this->pageinitialised) {
            return;
        }
        $this->pageinitialised = true;

        $PAGE->requires->js_amd_inline(<<<EOT
            require(['jquery'], function($) {
                $('.report_coursehealth_table').on('click', '.assign-turnaround-expand-actions', function(e) {
                    e.preventDefault();
                    var node = $(e.target).closest('.assign-turnaround-cell');
                    var content = node.find('.assign-turnaround-collapsible-content');
                    if (node.hasClass('collapsed')) {
                        content.show();
                        content.attr('aria-expanded', 'true');
                        node.removeClass('collapsed');
                    } else {
                        content.hide();
                        content.attr('aria-expanded', 'false');
                        node.addClass('collapsed');
                    }
                });
            });
EOT
        );
    }
}
