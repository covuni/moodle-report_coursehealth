<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

/*
 *
 * @package report
 * @subpackage coursehealth
 * @copyright 2018 Kevin Moore (Coventry University)
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once(__DIR__ . '/reportColumn.php');

/**
 * Count the number of echo activities in the course.
 *
 * @author Kevin Moore <ac4581@coventry.ac.uk> Coventry University
 *

 */
class echoUsage extends reportColumn
{

    public function __construct($context) {
        parent::__construct($context, 'echousage');
    }

    /**
     * Do whatever analysis is needed for this datapoint using this courseid.
     * Store the data in array indexed against the courseid.
     *
     * @param unknown $courseid

     */
    public function analyse($courseid, $fullname = '') {
        global $DB;

        $res = $DB->get_record_sql('SELECT count(*) as c from {lti}
                                    WHERE course=?',
                                    array( $courseid ), IGNORE_MISSING);

        parent::$data[$courseid][$this->key] = array(
            $this->key => $res->c
        );
    }

    public function decorate_table($courseid) {
        $datapoint = parent::$data[$courseid][$this->key][$this->key];

        if ($datapoint == - 1) { // Not used, but wont happen - would always be zero for no usage.
            return $this->notApplicableicon();
        } else {
            return $datapoint;
        }
    }

    public function decorate_xls($courseid) {
        $datapoint = parent::$data[$courseid][ $this->key ][$this->key];
        return $datapoint;
    }
}
