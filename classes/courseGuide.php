<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

/*
 *
 * @package report
 * @subpackage coursehealth
 * @copyright 2018 Kevin Moore (Coventry University)
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once(__DIR__ . '/reportColumn.php');

/**
 * "Module information" or Course guide is the /mod/courseguide activity plugin added to the course contents.
 * Stores the analysis as 1 for completed, 0 for not, -1 meaning the module information doesnt exist in the course.
 *
 * @author Kevin Moore <ac4581@coventry.ac.uk> Coventry University
 *
 */
abstract class courseGuide extends reportColumn
{
    private $guidename = ''; // Changed in CTOR.

    public function __construct($context, $key, $guidename) {
        parent::__construct($context, $key);
        $this->guidename = $guidename;
    }

    /**
     * Find the courseguide module on this course identified as 'Module information', see if has its 'completed' box ticked
     * (which isnt moodle's
     * Store the data in array indexed against the courseid.
     *
     * check lib/modinfolib.php for details of class cm_info
     *
     * @param unknown $courseid

     */
    public function analyse($courseid, $fullname='') {
        global $DB;

        $course = get_course($courseid);
        $modinfo = get_fast_modinfo($course);
        $ret = - 1; // Default to not present.

        if (array_key_exists('courseguide', $modinfo->get_used_module_names())) {
            $cminfos = $modinfo->get_instances_of('courseguide'); // Array of cm_info indexed by instance id.
            foreach ($cminfos as $cminfo) {
                if (strcasecmp(trim($cminfo->name), $this->guidename) == 0) {
                    $ret = $this->was_guide_populated($cminfo->instance) ? 1 : 0;
                    break;
                }
            }
        }
        return $ret;
    }

    /**
     * Whether the guide is completed.
     *
     * @param int $courseguideid The guide ID.
     * @return bool
     */
    public function is_guide_completed($courseguideid) {
        global $DB;
        return $DB->get_field('mod_courseguide_guides', 'completed', ['courseguideid' => $cminfo->instance]);
    }

    /**
     * Whether the guide was populated at all.
     *
     * @param int $courseguideid The guide ID.
     * @return bool
     */
    public function was_guide_populated($courseguideid) {
        global $DB;
        return $DB->record_exists('mod_courseguide_guides', ['courseguideid' => $courseguideid]);
    }

    /**
     * Derived classes need to implement
     */
    public function decorate_table($courseid) {
    }

    public function decorate_xls($courseid) {
    }
}

