<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package report
 * @subpackage coursehealth
 * @copyright 2018 Kevin Moore (Coventry University)
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace report_coursehealth\task;
defined('MOODLE_INTERNAL') || die();

/**
 * Executes the background tasks for this plugin, execute in a cron.
 *
 * For averageHits per user:
 * Counts the CRUD transactions on a course via the logstore and stores cumulative totals in own report_coursehealth tables.
 * This is done because it is potentially timeconsuming  and the logstore can be legitimately cleared down.
 * The cron only generates this data for courses listed in the report_coursehealth table.
 * This is to only bother with courses that need to be reported on.
 * The analytic code needs to insert an initial record or it wont be processed here.
 *
 * @author <ac4581@coventry.ac.uk> Kevin Moore
 *
 */
class coursehealthtask extends \core\task\scheduled_task {

    public function get_name() {
        // Shown in admin screens.
        return get_string('coursehealthtask_taskname', 'report_coursehealth');
    }

    public function execute() {
        global $DB;

        $now = time();

        // Get the roles. When the setting is not defined, role IDs are [0].
        $roleconfig = get_config('report_coursehealth', 'studentroleids');
        $roleids = array_map('intval', explode(',', (string) $roleconfig));

        list($roleidsql, $roleidparams) = $DB->get_in_or_equal($roleids, SQL_PARAMS_NAMED);
        $sql = "SELECT l.courseid, ch.id, COUNT(DISTINCT l.id) AS no, MAX(l.timecreated) AS lasteventtime
                  FROM {logstore_standard_log} l
                  JOIN {context} ctx
                    ON ctx.instanceid = l.courseid
                   AND ctx.contextlevel = :courselevel
                  JOIN {role_assignments} ra
                    ON ra.userid = l.userid
                   AND ra.contextid = ctx.id
                   AND ra.roleid $roleidsql
             LEFT JOIN {report_coursehealth} ch
                    ON ch.courseid = l.courseid
                 WHERE (ch.id IS NOT NULL AND l.timecreated > ch.last_update)
              GROUP BY l.courseid, ch.id";

        // To support the discovery of new courses, which have not yet been seen in the
        // report, we can add the following WHERE condition. The :sometime does not need
        // to exceed the frequency of the scheduled task, and 1 minute is certainly enough.
        // OR (ch.id IS NULL AND l.timecreated > :sometime)

        $params = $roleidparams + [
            'courselevel' => CONTEXT_COURSE
        ];

        $recordset = $DB->get_recordset_sql($sql, $params);
        foreach ($recordset as $data) {
            try {
                $this->insert_or_update_from_data($data, $now);
            } catch (\dml_exception $e) {
                mtrace("Error while saving course {$data->courseid} data: " . $e->getMessage());
            }
        }
        $recordset->close();
    }

    /**
     * Insert or update from data.
     *
     * @param stdClass $data Contains id, courseid, no, lasteventtime. ID may be null.
     * @param int $now The time at which the data was extracted.
     */
    protected function insert_or_update_from_data($data, $now) {
        global $DB;

        if (empty($data->id)) {
            $record = new \stdClass();
            $record->courseid = $data->courseid;
            $record->all_counter = $data->no;
            $record->last_update = $now;
            $DB->insert_record('report_coursehealth', $record);

        } else {
            $sql = "UPDATE {report_coursehealth}
                       SET all_counter = all_counter + :no,
                           last_update = :lastupdate
                     WHERE id = :id";
            $params = [
                'id' => $data->id,
                'no' => $data->no,
                'lastupdate' => max($now, $data->lasteventtime)
            ];
            $DB->execute($sql, $params);
        }
    }
}
