<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

/*
 *
 * @package report
 * @subpackage coursehealth
 * @copyright 2018 Kevin Moore (Coventry University)
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once(__DIR__ . '/reportColumn.php');
require_once(__DIR__ . '/courseGuide.php');

/**
 * "You Said We Did" or Course guide is the /mod/courseguide activity plugin added to the course contents.
 * Stores the analysis as 1 for completed, 0 for not, -1 meaning the module information doesnt exist in the course.
 *
 * @author Kevin Moore <ac4581@coventry.ac.uk> Coventry University
 *
 */
class yousaidwedid extends courseGuide
{
    public function __construct($context) {
        parent::__construct($context, 'yousaidwedid', 'You Said We Did');
    }

    /**
     * Do whatever analysis is needed for this datapoint using this courseid.
     * Store the data in array indexed against the courseid.
     *
     * @param unknown $courseid
     */
    public function analyse($courseid, $fullname='') {
        global $DB;

        $ret = parent::analyse($courseid, $fullname );

        // error_log("YSWD ret = $ret");

        parent::$data[$courseid][$this->key] = array(
            $this->key => $ret
        );
    }

    /**
     *
     * Implement abstract methods
     */
    public function decorate_table($courseid) {
        $datapoint = parent::$data[$courseid][ $this->key ][ $this->key ];

        if ($datapoint == - 1) { // No label.
            return $this->notApplicableicon();
        } else {
            return $this->returnCompletedicon($datapoint);
        }
    }

    public function decorate_xls($courseid) {
        $datapoint = parent::$data[$courseid][ $this->key ][ $this->key ];
        return $datapoint;
    }
}
