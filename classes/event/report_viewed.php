<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace report_coursehealth\event;
defined('MOODLE_INTERNAL') || die();

/*
 * Define the report_coursehealth report viewed event.
 * Aiming to add a log entry when a report is created.
 *
 * @package    report_coursehealth
 * @copyright 2018 Kevin Moore (Coventry University)
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * The report_coursehealth report viewed event class.
 *
 * @property-read array $other {
 *      Extra information about event.
 * }
 *
 */
class report_viewed extends \core\event\base {

    /**
     * Init method.
     *
     * @return void
     */
    protected function init() {
        $this->data['crud'] = 'r';
        $this->data['edulevel'] = self::LEVEL_OTHER;
    }

    /**
     * Return localised event name.
     *
     * @return string
     */
    public static function get_name() {
        return get_string('event.reportviewed', 'report_coursehealth');
    }

    /**
     * Returns description of what happened.
     *
     * @return string
     */
    public function get_description() {
        return "The user with id '$this->userid' viewed the report for category id '" . $this->other['category'] . "'";
    }

    /**
     * Get URL related to the action.
     *
     * @return \moodle_url
     */
    public function get_url() {
        return new \moodle_url('/report/coursehealth/index.php', array('id' => $this->contextinstanceid,
            'category' => $this->other['category']));
    }

    /**
     * Return the legacy event log data. Where is this used??
     *
     * @return array
     */
    protected function get_legacy_logdata() {
        return array($this->courseid, 'coursehealth', 'report', 'index.php?id=' . $this->contextinstanceid . '&category=' .
            $this->other['category']);
    }

    /**
     * Custom validation.
     *
     * @throws \coding_exception
     * @return void
     */
    protected function validate_data() {
        parent::validate_data();

        if (!isset($this->other['category'])) {
            throw new \coding_exception('The \'category\' value must be set in other.');
        }

    }

    public static function get_other_mapping() {
        $othermapped = array();
        return $othermapped;
    }
}
