<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'report_coursehealth', language 'en'
 *
 * @package report
 * @subpackage coursehealth
 * @copyright 2018 Kevin Moore (Coventry University)
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// These are table column titles.
$string['header.announcements'] = "Announcements";
$string['header.assignmentturnaround'] = "Assignment Turnaround";
$string['header.assignmentturnaroundcount'] = "Feedback released";
$string['header.assignmentturnarounddetails'] = "Assignment turnaround breakdown";
$string['header.averagehits'] = "Average hits per user";
$string['header.course'] = "Course";
$string['header.discussions'] = "Forum posts";
$string['header.echousage'] = "Echo usage";
$string['header.moderatorguideexternal'] = "Moderator Guide External Examiner";
$string['header.moderatorguidestaff'] = "Moderator Guide Staff";
$string['header.moduleinformation'] = "Module information";
$string['header.studentcount'] = "Students";
$string['header.visibility'] = "Visible";
$string['header.yousaidwedid'] = "You said we did";

// These are help icons on the table headers to explain the methodology for each column.
$string['header.announcements_help'] = "How many discussion threads have been started in the forums?";
$string['header.assignmentturnaround_help'] = "The average (mean) time between an assignment due date and the release of grades for submissions on that assignment. This only includes submissions that have feedback and where marking workflow is enabled and the submission is released. Any empty result implies none of the submission meet these conditions.";
$string['header.assignmentturnaroundcount_help'] = "What proportion of assignments on this course are using marking workflow and have feedback released?";
$string['header.assignmentturnarounddetails_help'] = "Breakdown of the turn around for each assignment.";
$string['header.averagehits_help'] = "The ratio of activity counts as seen in the logs over the number of users in the course. Higher values imply greater number of interactions per user.";
$string['header.course_help'] = "A link to the course";
$string['header.discussions_help'] = "The number of posts made across forums excluding the tutor only announcements forum.";
$string['header.echousage_help'] = "Count the number of times the echo activity is used";
$string['header.moderatorguideexternal_help'] = "help string for moderatorguide external";
$string['header.moderatorguidestaff_help'] = "Has the moderator guide for staff been marked as \"completed\"?";
$string['header.moduleinformation_help'] = "Has the module information activity been marked as \"completed\"?";
$string['header.studentcount_help'] = "The number of students in the course.";
$string['header.visibility_help'] = "Reports whether the course visibility is set to 'Show'.";
$string['header.yousaidwedid_help'] = "Has the \"You Said We Did\" label been updated?";

// Rollovers on table icons such as green ticks.
$string['rollover.notapplicable'] = "Not used";
$string['rollover.completed'] = "Completed";
$string['rollover.notcompleted'] = "Not completed";
$string['rollover.scheduled'] = "Statistics will be collected shortly";
$string['rollover.warning'] = "Warning, potential error";

// If a link is required on a column value in the table specify a decorator to decide where we link to.
$string['decorator.course'] = '/course/view.php?id={$a->id}';
$string['decorator.announcements'] = '/course/view.php?id={$a->id}';

// Form to specify the query.
$string['filtercategories'] = 'Categories';
$string['filtercategories_help'] = 'Select a category of courses to report on';
$string['filterfullname'] = 'Matching Filter';
$string['filterfullname_help'] = 'Specify a text filter on the course fullname eg 1718MAY. The course name must contain this string to be included in the report.';
$string['filtercoursehidden'] = 'Hidden';
$string['filtercoursetitle'] = 'Include courses if ...';

// Capability, Events.
$string['coursehealth:view'] = 'Create and view reports on course health';
$string['event.reportviewed'] = 'Course health report viewed';

// General strings.
$string['acolonb'] = '{$a->left}: {$a->right}';
$string['columnsettings'] = 'Column settings';
$string['coursehealthtask_taskname'] = 'Course health reporting';
$string['downloadexcel'] = "Download to Excel";
$string['generatereport'] = 'Generate report';
$string['includesubcats'] = 'Include sub categories';
$string['includewithoutstudents'] = 'No students are enrolled';
$string['pluginname'] = 'Course health reporting';
$string['turnaroundskipgreaterthan'] = 'Ignore turn around greater than';
$string['turnaroundskipgreaterthan_help'] = 'When computing the average assignment turn around of the course, we skip the assignments with a turn around greater than the duration provided. You may set this to 0 to include all assignments.';
$string['studentroles'] = 'Student roles';
$string['studentroles_desc'] = 'This settings tells the report which users are students. Any user with one of the above roles in the context of a course will be considered as a student. Note at least one role should be selected.';






