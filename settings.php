<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Settings.
 *
 * @package    report_coursehealth
 * @copyright  2018 Kevin Moore <ac4581@coventry.ac.uk> Coventry University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

$ADMIN->add('reports', new admin_externalpage('reportcoursehealth', get_string('pluginname', 'report_coursehealth'),
     new moodle_url('/report/coursehealth/index.php'), 'report/coursehealth:view'));

$settings = null;

if ($hassiteconfig) {
    $settings = new admin_settingpage('reportcoursehealthsettings', get_string('pluginname', 'report_coursehealth'));

    if ($ADMIN->fulltree) {
        $settings->add(new admin_setting_pickroles(
            'report_coursehealth/studentroleids',
            get_string('studentroles', 'report_coursehealth'),
            get_string('studentroles_desc', 'report_coursehealth'),
            ['student']
        ));
    }
}
