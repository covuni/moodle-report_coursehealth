<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Library file for this plugin.
 *
 * @package report
 * @subpackage coursehealth
 * @copyright 2018 Kevin Moore (Coventry University)
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Tells whether we support a certain logstore.
 *
 * @param tool_log\store $instance The log store instance.
 * @return bool
 */
function report_coursehealth_supports_logstore($instance) {
    // We don't use the store API to read the data, but we depend on its database storage.
    return $instance instanceof \logstore_standard\log\store;
}

/**
 * Callback from pluginfile.php to return a file  generated  by this plugin.
 * It must send the file and terminate. Whatever it returns leads to "not found".
 *
 * @param unknown $course
 * @param unknown $cm
 * @param unknown $context
 * @param unknown $filearea
 * @param unknown $args
 * @param unknown $forcedownload
 * @param array $options
 */

function report_coursehealth_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options = []) {

    // Check the contextlevel is as expected - if your plugin is a block, this becomes CONTEXT_BLOCK, etc.
    if ($context->contextlevel != CONTEXT_SYSTEM) {
        trigger_error( "context level was ". $context->contextlevel );
        return false;
    }

    // Make sure the filearea is one of those used by the plugin.
    if ($filearea !== 'xlsreport') {
        trigger_error( "filearea was ". $filearea );
        return false;
    }

    // Make sure the user is logged in and has access to the module
    // (plugins that are not course modules should leave out the 'cm' part).
    require_login($course, true);

    // Check the relevant capabilities - these may vary depending on the filearea being accessed.
    if (!has_capability('report/coursehealth:view', $context)) {
        return false;
    }

    // Leave this line out if you set the itemid to null in make_pluginfile_url (set $itemid to 0 instead).
    $itemid = array_shift($args); // The first item in the $args array.
    // trigger_error( "its OK but itemid was ". $itemid );

    // Use the itemid to retrieve any relevant data records and perform any security checks to see if the
    // User really does have access to the file in question.

    // Extract the filename / filepath from the $args array.
    $filename = array_pop($args); // The last item in the $args array.
    if (!$args) {
        $filepath = '/'; // $args is empty => the path is '/'
    } else {
        $filepath = '/'.implode('/', $args).'/'; // $args contains elements of the filepath
    }
    // trigger_error( "its OK but filepath was ". $filepath );

    // Retrieve the file from the Files API.
    $fs = get_file_storage();
    $file = $fs->get_file($context->id, 'report_coursehealth', $filearea, $itemid, $filepath, $filename);
    if (!$file) {
        // trigger_error( "file doesnt exist at get_file ". print_r($file,true) );
        return false; // The file does not exist.
    }

    // We can now send the file back to the browser - in this case with a cache lifetime of 1 day and no filtering.
    // From Moodle 2.3, use send_stored_file instead.
    send_stored_file($file, 86400, 0, true, []);
}
