<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/*
 * This index page is the main displayed page.
 * It displays the form that sets-up the report and displays the results.
 * it primary concern is display and forms, actually collecting data is done in other classes.
 *
 * @package report
 * @subpackage coursehealth
 * @copyright 2018 Kevin Moore (Coventry University)
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require(dirname(__FILE__) . '/../../config.php');
require_once($CFG->libdir . '/adminlib.php');
require_once($CFG->libdir . '/tablelib.php');
require_once(__DIR__ . '/classes/coursehealthform.php');
require_once(__DIR__ . '/classes/coursehealthreport.php');
require_once(__DIR__ . '/classes/event/report_viewed.php');

$reportparams = [];
$filterformsubmitted = optional_param('filterformsubmitted', 0, PARAM_INT);
$download = optional_param('download', null, PARAM_ALPHANUMEXT);

// Due to the way the page is setup, we need a basic check to see if the form was
// submitted. These optional_param capture the values sent by the form and bypass
// its validation and data massaging.
if (!$filterformsubmitted) {
    $reportparams = array_reduce(coursehealthform::$reportkeys, function($carry, $data) {
        list($key, $type, $default) = $data;
        $carry[$key] = optional_param($key, $default, $type);
        return $carry;
    }, []);
}

$baseurl = new moodle_url('/report/coursehealth/index.php');
$url = new moodle_url($baseurl, $reportparams);
$context = context_system::instance();

admin_externalpage_setup('reportcoursehealth', '', null, $url, ['pagelayout' => 'report']);

// Now output a form to collect the query.
$mform = new coursehealthform();

if ($mform->is_cancelled()) {
    redirect($baseurl);

} else if ($submitteddata = $mform->get_data()) {

    // Trigger a report viewed event, passing in the query.
    $conditions = get_object_vars($submitteddata);
    $event = \report_coursehealth\event\report_viewed::create([
        'context' => $context,
        'relateduserid' => 0,
        'other' => $conditions
    ]);
    $event->trigger();

    // Create the URL for the report.
    $url = new moodle_url($baseurl, array_reduce(coursehealthform::$reportkeys, function($carry, $data) use ($submitteddata) {
        $key = $data[0];
        $carry[$key] = $submitteddata->{$key};
        return $carry;
    }, []));
    redirect($url);

} else if (empty($reportparams['category'])) {
    echo $OUTPUT->header();
    echo $OUTPUT->heading(get_string('pluginname', 'report_coursehealth'));
    $mform->display();
    echo $OUTPUT->footer();
}

// Run the query to collect results.
$results = new coursehealthreport($context, (object) $reportparams);

if (!empty($download)) {
    $results->table($url, $download);
    die();
}

echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('pluginname', 'report_coursehealth'));
$results->table($url, $download);
echo $OUTPUT->footer();
