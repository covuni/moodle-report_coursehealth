# README #

Moodle Module report course health

### What is this repository for? ###

* A report primarily for managers to be able to query modules in a given faculty (category) to get an overview of how consistently Moodle is being used on those modules.
* Report should be available to anyone assigned the appropriate moodle capability.

### License ###

Licensed under the GNU GPL v3.

### Installation ###

Installs into <moodleroot>/reports/coursehealth
Becomes available under <site admin>/reports/course health



### Role and capability ###
* Requires the user creating reports has a capability "report/coursehealth:view"

### Logging ###
TODO Generating reports creates a log in Moodle's activity logs. The component is "Course health reporting" and the Event name is "Course health report viewed"


### Usage ###

* Use the dropdown to select a faculty / category to report on. The aim is to prevent time consuming site-wide reports.
* Provide the optional text as an additional filter for example "OCTJAN" to limit the scope of the report. 
* Click the Generate report button to view the report.
* An Excel spreadsheet is available to download.

### Report Information / columns ###
The columns reported are listed below, see the document in docs/columnDetails.docx for details of how each column is calculated.

* Module name hyper linked to module
* You said we did complete (will also use course guide plugin - requires some more dev work)
* Average hits per user
* Moderator Guide Staff completion flag
* Module information complete
* Number of announcements 
* Assignment turnaround
* Echo usage

### Populating Database Table ###

* The average hits per user requires queries of the log tables. To speed this up this is done in a cron and only for courses included in a report. This means a new report 
will likely have courses that have no data yet collected by the cron, these appear as a grey clock icon with a "statistics will be collected shortly" rollover. The cron only processes
those parts of the logs since the last updated time which also has the advantage of continuing to accumulate data if the logs are cleared down.
 

### Web Service ###

* TODO Create web service to allow information to be extracted by other reporting tools